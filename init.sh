#!/bin/bash
yum install -y nginx
systemctl enable nginx
systemctl start nginx

# Downloading of s3 objects for frontend
aws s3api get-object --bucket haha-counter-frontend --key index.html index.html
aws s3api get-object --bucket haha-counter-frontend --key main.css main.css
aws s3api get-object --bucket haha-counter-frontend --key main.js main.js
mv index.html /usr/share/nginx/html/index.html
mv main.css /usr/share/nginx/html/main.css
mv main.js /usr/share/nginx/html/main.js