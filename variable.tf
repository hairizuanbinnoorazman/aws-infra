variable "name_prefix" {
    description = "Name prefix to be prepended on created resources to make it easier to identify on AWS Console"
    type        = string
    default     = "counter"
}

variable "instance_type" {
    description = "Instance type of EC2 instances"
    type        = string
    default     = "t2.micro"
}

variable "ssh_key_pair_name" {
    description = "Define name of ssh key pair to allow ssh into instances"
    type        = string
}