//initialising a variable name data
 
let data = 0;
 
//printing default value of data that is 0 in h2 tag
document.getElementById("counting").innerText = data;

// update backend
function update(val) {
    fetch("/update", {
        method: "POST",
        body: JSON.stringify({"data": val}),
        headers: {"Content-type": "application/json; charset=UTF-8"},
    })
      .then((response) => console.log(response));
}

//creation of increment function
function increment() {
    data = data + 1;
    document.getElementById("counting").innerText = data;
    update(data)
}
//creation of decrement function
function decrement() {
    data = data - 1;
    document.getElementById("counting").innerText = data;
    update(data)
}