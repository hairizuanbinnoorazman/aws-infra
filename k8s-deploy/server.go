package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"text/template"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type StatusHandler struct {
	StatusType string
}

func (s StatusHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Infof("Status Handler: %v started", s.StatusType)
	zz := map[string]string{"status": "ok"}
	aa, _ := json.Marshal(zz)
	w.WriteHeader(http.StatusOK)
	w.Write(aa)
}

type index struct {
	Username string
}

func (i index) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("begin index endpoint")
	defer log.Println("end index endpoint")

	tmpl := template.Must(template.ParseFiles("templates/main.html"))
	type indexData struct {
		Username string
	}
	tmpl.Execute(w, indexData{i.Username})
}

type CounterHandler struct {
	mgr counterMgr
}

func (c CounterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("begin counter update endpoint")
	defer log.Println("end counter update endpoint")

	rawData, _ := io.ReadAll(r.Body)
	type counterRequest struct {
		Data int `json:"data"`
	}
	var counterData counterRequest
	json.Unmarshal(rawData, &counterData)
	err := c.mgr.Update(counterData.Data)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("unable to update database: %v", err)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))

}

func main() {
	log.Println("start server")

	username, exists := os.LookupEnv("USERNAME")
	if !exists {
		log.Fatal("USERNAME environment variable not set")
	}

	dbUser, exists := os.LookupEnv("DB_USER")
	if !exists {
		log.Fatal("DB_USER environment variable not set")
	}

	dbPassword, exists := os.LookupEnv("DB_PASSWORD")
	if !exists {
		log.Fatal("DB_PASSWORD environment variable not set")
	}

	dbHost, exists := os.LookupEnv("DB_HOST")
	if !exists {
		log.Fatal("DB_HOST environment variable not set")
	}

	connectionString := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=True",
		dbUser,
		dbPassword,
		dbHost,
		3306,
		"counter",
	)
	db, err := gorm.Open(mysql.Open(connectionString), &gorm.Config{})
	if err != nil {
		log.Fatalf("unable to connect to database %v", err)
	}
	db.AutoMigrate(&counter{})

	r := mux.NewRouter()
	r.Handle("/", index{Username: username}).Methods("GET")
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	r.Handle("/healthz", StatusHandler{StatusType: "healthz"}).Methods("GET")
	r.Handle("/readyz", StatusHandler{StatusType: "readyz"}).Methods("GET")
	r.Handle("/metrics", promhttp.Handler()).Methods("GET")
	r.Handle("/update", CounterHandler{counterMySQL{db: db}})
	http.ListenAndServe(":8080", r)
}
