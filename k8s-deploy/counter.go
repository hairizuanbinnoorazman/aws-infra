package main

import (
	"time"

	"gorm.io/gorm"
)

type counter struct {
	Value      int
	UpdateTime time.Time
}

type counterMgr interface {
	Update(val int) error
}

type counterMySQL struct {
	db *gorm.DB
}

func (c counterMySQL) Update(val int) error {
	result := c.db.Create(&counter{Value: val, UpdateTime: time.Now()})
	if result.Error != nil {
		return result.Error
	}
	return nil
}
