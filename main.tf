terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.24.0"
    }

    local = {
      source = "hashicorp/local"
      version = "2.4.0"
    }
  }
}

provider "local" {}

provider "aws" {
  profile = "default"
}

data "aws_vpc" "default_vpc" {
    default = true
}

data "aws_subnet" "default_subnet" {
  vpc_id = data.aws_vpc.default_vpc.id
  availability_zone = "ap-southeast-1a"
}

data "aws_internet_gateway" "default_internet_gw" {
  filter {
    name   = "attachment.vpc-id"
    values = [data.aws_vpc.default_vpc.id]
  }
}

resource "aws_subnet" "private_vpc_subnetwork" {
  vpc_id     = data.aws_vpc.default_vpc.id
  cidr_block = "172.31.48.0/20"
  tags = {
    Name = "${var.name_prefix}-private-vpc"
  }
}

resource "aws_eip" "nat" {
  domain   = "vpc"

  tags = {
    Name = "${var.name_prefix}-nat"
  }
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = data.aws_subnet.default_subnet.id

  tags = {
    Name = "${var.name_prefix}-nat-gw"
  }
}

resource "aws_route_table" "private" {
  vpc_id = data.aws_vpc.default_vpc.id

  route {
    cidr_block = data.aws_vpc.default_vpc.cidr_block
    gateway_id = "local"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw.id
  }

  tags = {
    Name = "${var.name_prefix}-rt"
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private_vpc_subnetwork.id
  route_table_id = aws_route_table.private.id
}

data "aws_ami" "amazon" {
  most_recent = true
  filter {
    name   = "name"
    values = ["*ami-2023*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  owners = ["amazon"] # Canonical
}

resource "aws_security_group" "counter_sg" {
  tags = {
    Name = "${var.name_prefix}-sg"
  }
  description = "Test"
  vpc_id      = data.aws_vpc.default_vpc.id

  # For debugging purposes
  # ingress {
  #   description      = "Allow SSH ingress"
  #   from_port        = 22
  #   to_port          = 22
  #   protocol         = "tcp"
  #   cidr_blocks      = ["0.0.0.0/0"]
  #   ipv6_cidr_blocks = ["::/0"]
  # }

  ingress {
    description      = "Allow HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  # For debugging purposes
  # ingress {
  #   description      = "Allow ICMP"
  #   from_port        = -1
  #   to_port          = -1
  #   protocol         = "icmp"
  #   cidr_blocks      = ["0.0.0.0/0"]
  #   ipv6_cidr_blocks = ["::/0"]
  # }

  egress {
    description      = "Allow All traffic to go out"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_iam_instance_profile" "counter_profile" {
  name = "${var.name_prefix}_profile"
  role = aws_iam_role.counter_role.name
}

data "aws_iam_policy_document" "s3_get_access" {
  statement {
    effect = "Allow"
    actions = ["s3:GetObject"]
    resources = [ "arn:aws:s3:::*" ]
  }
}

data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "counter_role" {
  name               = "${var.name_prefix}-role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json
  inline_policy {
    name = "s3permissions"
    policy = data.aws_iam_policy_document.s3_get_access.json
  }
}

data "local_file" "init_script" {
  filename = "${path.module}/init.sh"
}

# For debugging purpose
# resource "aws_instance" "test" {
#   ami           = data.aws_ami.amazon.id
#   instance_type = var.instance_type
#   subnet_id     = aws_subnet.private_vpc_subnetwork.id
#   key_name      = var.ssh_key_pair_name

#   iam_instance_profile = aws_iam_instance_profile.counter_profile.name

#   vpc_security_group_ids = [ aws_security_group.test.id ]

#   tags = {
#     Name = "test"
#   }

#   user_data = data.local_file.init_script.content
#   user_data_replace_on_change = true
# }

# For debugging purposes
# resource "aws_instance" "public-test" {
#   ami           = data.aws_ami.amazon.id
#   instance_type = var.instance_type
#   subnet_id     = data.aws_subnet.default_subnet.id
#   key_name      = var.ssh_key_pair_name

#   vpc_security_group_ids = [ aws_security_group.counter_sg.id ]

#   tags = {
#     Name = "public-test"
#   }
# }

resource "aws_launch_template" "counter_frontend" {
  name_prefix = "${var.name_prefix}-frontend"
  image_id =  data.aws_ami.amazon.id
  instance_type = var.instance_type
  network_interfaces {
    subnet_id       = aws_subnet.private_vpc_subnetwork.id
    security_groups = [aws_security_group.counter_sg.id]
  }
  key_name      = var.ssh_key_pair_name
  iam_instance_profile {
    name = aws_iam_instance_profile.counter_profile.name
  }
  # vpc_security_group_ids = [ aws_security_group.test.id ]
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "${var.name_prefix}-frontend"
    }
  }

  user_data = data.local_file.init_script.content_base64
}

resource "aws_autoscaling_group" "counter_frontend_ag" {
  # availability_zones = ["asia-southeast-1a", "asia-southeast-1b", "asia-southeast-1c"]
  desired_capacity   = 3
  max_size           = 4
  min_size           = 2

  launch_template {
    id      = aws_launch_template.counter_frontend.id
    version = aws_launch_template.counter_frontend.latest_version
  }
  name = "${var.name_prefix}-frontend-ag"
}

// https://github.com/hashicorp/terraform-provider-aws/issues/32489
# data "aws_subnets" "all_public_subnets" {
#   filter {
#     name   = "default-for-az"
#     values = [true] 
#   }
# }

data "aws_subnet" "alternative_subnet" {
  vpc_id = data.aws_vpc.default_vpc.id
  availability_zone = "ap-southeast-1b"
  default_for_az = true
}

data "aws_subnet" "alternative_subnet_2" {
  vpc_id = data.aws_vpc.default_vpc.id
  availability_zone = "ap-southeast-1c"
  default_for_az = true
}

resource "aws_lb" "counter_frontend_alb" {
  name               = "${var.name_prefix}-frontend-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.counter_sg.id]
  # subnets            = [for subnet in data.aws_subnets.all_public_subnets : subnet.id]
  subnets = [ data.aws_subnet.default_subnet.id, data.aws_subnet.alternative_subnet.id, data.aws_subnet.alternative_subnet_2.id ]

  enable_deletion_protection = false
}

resource "aws_lb_target_group" "counter_frontend_alb_target_group" {
  name     = "${var.name_prefix}-frontend-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default_vpc.id
  deregistration_delay = 15
}

resource "aws_autoscaling_attachment" "counter_frontend_attach_alb" {
  autoscaling_group_name = aws_autoscaling_group.counter_frontend_ag.id
  lb_target_group_arn    = aws_lb_target_group.counter_frontend_alb_target_group.arn
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.counter_frontend_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.counter_frontend_alb_target_group.arn
  }
}

resource "aws_lb_listener_rule" "static" {
  listener_arn = aws_lb_listener.front_end.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.counter_frontend_alb_target_group.arn 
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}