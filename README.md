# aws-infra

The following project showcases 2 sample scenarios:

- Situation 1:
  - Deploying a nginx with a simple html, css, js behind a load balancer
  - Gitlab CI that would update the html, css and js accordingly
- Situation 2:
  - A sample kubernetes deployment

## Getting started (Situation 1)

If it's your first time setting this up, run the following command:

```bash
terraform init
```

Next, run the following commands:

```bash
terraform plan -out output.plan
terraform apply output.plan
```

To destroy created resources

```bash
terraform plan -out destroy.plan -destroy
terraform apply destroy.plan
```

## Frontend preview (Situation 1)

Initial frontend looks like this:

![initial page](initial-page.png)

After alteration (updated via CI):

![final page](final-page.png)

## Afterthoughts (Situation 1)

- Ideal would be to rely on one's own subnet (generated via terraform) rather than relying on default.
- Consider setting up rules to restrict traffic to only from other subnet for the private instances
- Maybe might be good to consider tools such as packer to "bake" such configurations - so that there is less of a need to setup instance profiles that require read access to s3 etc

## Deployment of counter app on k8s (Situation 2)

The following counter-app has been retrofitted with the Golang programming language (in order to have the frontend do some simple back and forth communication to some backend)

### Deployment steps

Run the following commands in the following order. There are comments in the following code block to dictate what each command does.

```bash
# Go into the folder which contains all the code. Commands has only been configured from this directory
cd k8s-deploy

# Start minikube cluster with appropiate port-exposures - so that can access locally.
# This was testing on WSL 2 on windows. Different setups may face different issues
make start-cluster

# Build the counter-app image in a docker image
make build-images

# Upload image into an internal docker registry within the minikube that was set up earlier. 
# Reduces the burden of doing additional work to get it to communicate with other private registries
make upload-images

# Deploy kube-prometheus-stack
make deploy-monitoring

# Apply relevant k8s manifest files
make apply-manifests
```

### Debugging steps

You will notice that counter-app will go through crash loop back initially. This is not too big of an issue. The reason for this is that it takes a while for database to start (sometimes, potential 1-2 minutes). During that point of time, the counter-app will simply fail and crash if it fails to start. 

However, due to k8s deployment model, the application will automatically recover after a while. Simply watch for the pods to be brought back alive once more

```bash
Every 2.0s: kubectl get pods                    xxx: Fri Nov 17 11:14:46 2023

NAME                           READY   STATUS             RESTARTS        AGE
counter-app-599d56ff98-sngmr   0/1     CrashLoopBackOff   3 (4m54s ago)   31m
database-64496476b8-qt2nq      1/1     Running            3 (2m5s ago)    37m
```

Additional notes:  
- Registry addon with docker driver uses port 32775 please use that instead of default port 5000

### Wishlist

Unfortunately, this project is extremely far from perfect - there is a lot of effort needed to get it up to par with production grade applications out there. Some of the example things that would be great to be introduced:

- Skaffold configs - ease of testing stuff (faster iteration loops during dev work)
- Helm chart - encapsulate counter-app in helm chart
- Use open source helm chart out there such as bitname mysql charts for proper primary-secondary replication
- Ensure proper security configurations in place - e.g. security context etc

## Learning Points

- To debug instances in private subnetwork, best to also setup a "bastion"/jump server in order to allow quick temporary access to it (since in this case, no requirement to include bastion host)
- To jump into the private server, the following commands might be useful:
  - `eval ``ssh-agent`` `
  - `ssh-add -l`
  - `ssh-add ~/.ssh/id_rsa` or other secret key
  - `ssh -A ec2-user@<ip address>` - don't forget the `A` flag
- When checking "init" scripts, check the following location `/var/log/cloud-init-output.log`
